import PostDto from './dto/post.dto';
import { PostEntity } from './post.entity';

export const toPostDto = (post: PostEntity): PostDto => {
	const { id, title, content } = post;
	const postDto: PostDto = { id, title, content };
	return postDto;
};
