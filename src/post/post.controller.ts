import {
	Body,
	Controller,
	Delete,
	Get,
	Param,
	Post,
	Put,
} from '@nestjs/common';
import PostService from './post.service';
import CreatePostDto from './dto/post-create.dto';
import UpdatePostDto from './dto/post-update.dto';
import PostDto from './dto/post.dto';

@Controller('post')
export default class PostController {
	constructor(private readonly postService: PostService) {}

	@Post()
	createPost(@Body() post: CreatePostDto): Promise<PostDto> {
		return this.postService.createPost(post);
	}

	@Get()
	getAllPosts(): Promise<PostDto[]> {
		return this.postService.getAllPosts();
	}

	@Get(':id')
	getPostById(@Param('id') id: string): Promise<PostDto> {
		return this.postService.getPostById(Number(id));
	}

	@Put(':id')
	async updatePost(
		@Param('id') id: string,
		@Body() post: UpdatePostDto,
	): Promise<PostDto> {
		return this.postService.updatePost(Number(id), post);
	}

	@Delete(':id')
	async deletePost(@Param('id') id: string) {
		this.postService.deletePost(Number(id));
	}
}
