import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import CreatePostDto from './dto/post-create.dto';
import UpdatePostDto from './dto/post-update.dto';
import PostDto from './dto/post.dto';
import { PostEntity } from './post.entity';
import { toPostDto } from './post.mapper';

@Injectable()
export default class PostService {
	constructor(
		@InjectRepository(PostEntity)
		private postRepository: Repository<PostEntity>,
	) {}

	async createPost(postBody: CreatePostDto): Promise<PostDto> {
		const post = this.postRepository.create(postBody);
		await this.postRepository.save(post);
		return toPostDto(post);
	}

	async getAllPosts(): Promise<PostDto[]> {
		const posts = await this.postRepository.find();
		return posts.map((post) => toPostDto(post));
	}

	async getPostById(id: number): Promise<PostDto> {
		const post = await this.postRepository.findOne(id);
		if (!post) {
			throw new NotFoundException(`Not found post with id ${id}.`);
		}
		return toPostDto(post);
	}

	async updatePost(id: number, updateBody: UpdatePostDto): Promise<PostDto> {
		const post = await this.postRepository.findOne(id);
		if (!post) {
			throw new NotFoundException(`Not found post with id ${id}.`);
		}

		Object.assign(post, updateBody);
		await post.save();
		return toPostDto(post);
	}

	async deletePost(id: number) {
		const deleteResonse = await this.postRepository.delete(id);
		if (!deleteResonse.affected) {
			throw new NotFoundException(`Not found post with id ${id}.`);
		}
	}
}
