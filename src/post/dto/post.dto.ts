export default class PostDto {
	id: string;
	title: string;
	content: string;

	createdAt?: Date;
	updatedAt?: Date;
}
