import { IsString, IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export default class UpdatePostDto {
	@IsOptional()
	@IsNumber()
	id?: number;

	@IsOptional()
	@IsString()
	title?: string;

	@IsOptional()
	@IsString()
	content?: string;
}
