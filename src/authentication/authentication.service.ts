import {
	BadRequestException,
	InternalServerErrorException,
} from '@nestjs/common';
import PostgresErrorCode from 'src/database/postgresErrorCode.enum';
import { UserService } from 'src/user/user.service';
import RegisterDto from './dto/register.dto';

export class AuthenticationService {
	constructor(private readonly userService: UserService) {}

	public async register(registrationData: RegisterDto) {
		try {
			const user = await this.userService.create(registrationData);
			return user;
		} catch (err) {
			if (err?.code === PostgresErrorCode.UniqueViolation) {
				throw new BadRequestException(
					`Email ${registrationData.email} is already in use`,
				);
			}
			throw new InternalServerErrorException('Something went wrong');
		}
	}

	public async getAuthenticatedUser(email: string, password: string) {
		const user = await this.userService.login({ email, password });
		return user;
	}
}
