import UserDto from './dto/user.dto';
import { UserEntity } from './user.entity';

export const toUserDto = (user: UserEntity): UserDto => {
	const { id, email, name } = user;
	const userDto: UserDto = { id, email, name };
	return userDto;
};
