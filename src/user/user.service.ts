import {
	BadRequestException,
	Injectable,
	NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity';
import CreateUserDto from './dto/user-create.dto';
import LoginUserDto from './dto/user-login.dto';
import UserDto from './dto/user.dto';
import { toUserDto } from './user.mapper';
import UpdateUserDto from './dto/user-update.dto';

@Injectable()
export class UserService {
	constructor(
		@InjectRepository(UserEntity)
		private userRepository: Repository<UserEntity>,
	) {}

	async create(userBody: CreateUserDto): Promise<UserDto> {
		const user = this.userRepository.create(userBody);
		await this.userRepository.save(user);
		return toUserDto(user);
	}

	async login({ email, password }: LoginUserDto): Promise<UserDto> {
		const user = await this.userRepository.findOne({ where: { email } });
		if (!user || user.comparePassword(password)) {
			throw new BadRequestException(`Invalid email or password.`);
		}
		return toUserDto(user);
	}

	async getByEmail(email: string): Promise<UserEntity> {
		const user = await this.userRepository.findOne({ email });
		if (!user) {
			throw new NotFoundException(`Not found user with email ${email}.`);
		}
		return user;
	}

	async findById(id: string): Promise<UserDto> {
		const user = await this.userRepository.findOne(id);
		if (!user) {
			throw new NotFoundException(`Not found user with id ${id}.`);
		}
		return toUserDto(user);
	}

	async findAll(): Promise<UserDto[]> {
		const users = await this.userRepository.find();
		return users.map((user) => toUserDto(user));
	}

	async updateById(id: string, updateBody: UpdateUserDto): Promise<UserDto> {
		const user = await this.userRepository.findOne(id);
		if (!user) {
			throw new NotFoundException(`Not found user with id ${id}.`);
		}

		Object.assign(user, updateBody);
		await user.save();
		return toUserDto(user);
	}
}
