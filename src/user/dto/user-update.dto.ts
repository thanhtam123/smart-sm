import { IsEmail, IsOptional, IsString, MinLength } from 'class-validator';

export default class UpdateUserDto {
	@IsOptional()
	@IsEmail()
	email?: string;

	@IsOptional()
	name?: string;

	@IsOptional()
	@IsString()
	@MinLength(8)
	password?: string;
}
