import {
	IsEmail,
	IsNotEmpty,
	IsOptional,
	IsString,
	MinLength,
} from 'class-validator';

export default class CreateUserDto {
	@IsNotEmpty()
	@IsEmail()
	email: string;

	@IsOptional()
	@IsString()
	name?: string;

	@IsNotEmpty()
	@IsString()
	@MinLength(8)
	password: string;
}
