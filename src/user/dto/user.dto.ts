export default class UserDto {
	id?: string;

	email: string;

	name?: string;

	password?: string;

	createdAt?: Date;

	updatedAt?: Date;
}
