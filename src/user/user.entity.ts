import {
	BaseEntity,
	BeforeInsert,
	BeforeUpdate,
	Column,
	CreateDateColumn,
	Entity,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { IsString, MinLength } from 'class-validator';

@Entity('customer')
export class UserEntity extends BaseEntity {
	@PrimaryGeneratedColumn('uuid')
	public id?: string;

	@Column({ type: 'varchar', nullable: false, unique: true })
	public email: string;

	@Column({ type: 'varchar' })
	public name: string;

	@Column({ type: 'varchar', nullable: false })
	@IsString()
	@MinLength(8)
	public password: string;

	@CreateDateColumn({
		type: 'timestamp',
		default: () => 'CURRENT_TIMESTAMP(6)',
	})
	createdAt: Date;

	@UpdateDateColumn({
		type: 'timestamp',
		default: () => 'CURRENT_TIMESTAMP(6)',
		onUpdate: 'CURRENT_TIMESTAMP(6)',
	})
	updatedAt: Date;

	@BeforeInsert()
	@BeforeUpdate()
	async hashPassword(): Promise<void> {
		this.password = await bcrypt.hash(this.password, 10);
	}

	async comparePassword(password: string): Promise<Boolean> {
		return await bcrypt.compare(password, this.password);
	}
}
